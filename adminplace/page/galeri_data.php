<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Data Galeri";
	}
	GantiTitle();
</script>

<?php
include "galeri_aksi.php";
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Galeri</li>
		<li class="active">Data Galeri</li>
	</ol>
	
	<h3 align="center">Data Galeri</h3>
	
	<br/>
	
	<p><a class="btn btn-default" href="index.php?link=page/galeri_tambah.php"><i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Tambah Galeri</a></p>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/galeri_data.php">
		<div class="scrolling">
			<table class="table table-bordered table-hover data_tanggal">
				<thead>
					<tr>
						<th>Tanggal & Waktu</th>
						<th>Gambar</th>
						<th>Nama</th>
						<th>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				$ambil_data = mysql_query("SELECT * FROM galeri");
				while($hasil_data = mysql_fetch_array($ambil_data))
				{
				?>

					<tr>
						<td style="vertical-align: middle;"><p><input class="terpilih" type="checkbox" name="terpilih[]" value="<?=$hasil_data['id_galeri'];?>" style="padding-right">&nbsp;&nbsp;<?=$hasil_data['tanggal_post'];?> <?=$hasil_data['jam'];?></p></td>
						<td align="center" style="vertical-align: middle;"><img class="img-thumbnail" data-src="holder.js/250x200" alt="Gambar" <?php if(empty($hasil_data['gambar'])){echo  "src='../gambar/gambar_kosong.jpg'";}else{echo "src='../gambar/galeri/thumb_" . $hasil_data['gambar'] . "'";}?> width="150"/></td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['nama'];?></p></td>
						<td align="center" style="vertical-align: middle;">
							<a class="btn btn-sm btn-warning tip-button" data-toggle="tooltip" data-original-title="Edit" href="index.php?link=page/galeri_edit.php&id=<?=$hasil_data['id_galeri'];?>"><span class="glyphicon glyphicon-edit"></span></a>
							<button type="submit" class="btn btn-sm btn-danger tip-button" data-toggle="tooltip" data-original-title="Hapus"  name="hapus_galeri" value="<?=$hasil_data['id_galeri'];?>" onclick="return confirm('Yakin Hapus Data?');"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				
				<?php
				}
				?>
				</tbody>
			</table>
			<br/>
		</div>
	
		<hr/>
		
		<p align="center">
			<button type="submit" id="hapus_terpilih" class="btn btn-danger" name="hapus_terpilih" onclick="return confirm('Yakin Hapus Data Yang Terpilih?');" style="display: none;">
				<span class="glyphicon glyphicon-trash" style="padding-right: 10px;"></span>Hapus <span id="hitung_terpilih"></span>
			</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>
	
</div>