<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Beranda Admin";
	}
	GantiTitle();
</script>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li class="active">Beranda Admin</li>
	</ol>

	<h3 align="center">Selamat Datang <?=$ambil_data_adminweb['nama_lengkap'];?></h3>

	<hr/>

	<?php
	$jumlah_profil = mysql_num_rows(mysql_query("SELECT * FROM profil"));
	$jumlah_info_sekolah = mysql_num_rows(mysql_query("SELECT * FROM info_sekolah"));
	$jumlah_artikel = mysql_num_rows(mysql_query("SELECT * FROM artikel"));
	$jumlah_jadwal_kegiatan = mysql_num_rows(mysql_query("SELECT * FROM jadwal_kegiatan"));
	$jumlah_galeri = mysql_num_rows(mysql_query("SELECT * FROM galeri"));
	$jumlah_file = mysql_num_rows(mysql_query("SELECT * FROM file"));
	$jumlah_pesan = mysql_num_rows(mysql_query("SELECT * FROM pesan"));
	
	$total_data = $jumlah_profil+$jumlah_info_sekolah+$jumlah_artikel+$jumlah_jadwal_kegiatan+$jumlah_galeri+$jumlah_file;
	?>

	<div class="row">
	
		<div class="col-lg-12">
			<div id="grafik"></div>
				<script type="text/javascript">
					Highcharts.chart('grafik', {
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						title: {
							text: 'Grafik Rincian Total Data'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: true,
									format: '<b>{point.name}</b>: {point.percentage:.1f} %',
									style: {
										color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
									}
								}
							}
						},
						series: [{
							name: 'Total',
							colorByPoint: true,
							data: [
								<?php
								$persentase_profil = ($total_data * $jumlah_profil / 100);
								$persentase_info_sekolah = ($total_data * $jumlah_info_sekolah / 100);
								$persentase_artikel = ($total_data * $jumlah_artikel / 100);
								$persentase_jadwal_kegiatan = ($total_data * $jumlah_jadwal_kegiatan / 100);
								$persentase_galeri = ($total_data * $jumlah_galeri / 100);
								$persentase_file = ($total_data * $jumlah_file / 100);
								echo "
								{
									name: 'Profil (Total Data : $jumlah_profil) ',
									y: $persentase_profil
								},
								{
									name: 'Info Sekolah (Total Data : $jumlah_info_sekolah) ',
									y: $persentase_info_sekolah
								},
								{
									name: 'Artikel (Total Data : $jumlah_artikel) ',
									y: $persentase_artikel
								},
								{
									name: 'Jadwal Kegiatan (Total Data : $jumlah_jadwal_kegiatan) ',
									y: $persentase_jadwal_kegiatan
								},
								{
									name: 'Galeri (Total Data : $jumlah_galeri) ',
									y: $persentase_galeri
								},
								{
									name: 'File (Total Data : $jumlah_file) ',
									y: $persentase_file
								},
								";
								?>
							]
						}]
					});
				</script>
			</div>
			
	</div>
	
	<br/>
	
	<div class="row">
	
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-building-alt fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_profil;?> Data</h3>
							<h4 align="right">Profil</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/profil_data.php">Lihat Data Profil<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-info-circle fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_info_sekolah;?> Data</h3>
							<h4 align="right">Info Sekolah</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/info_sekolah_data.php">Lihat Data Info Sekolah<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
			
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-paper fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_artikel;?> Data</h3>
							<h4 align="right">Artikel</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/artikel_data.php">Lihat Data Artikel<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
			
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-tasks-alt fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_jadwal_kegiatan;?> Data</h3>
							<h4 align="right">Jadwal Kegiatan</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/artikel_data.php">Lihat Data Jadwal Kegiatan<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-image fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_galeri;?> Data</h3>
							<h4 align="right">Galeri</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/galeri_data.php">Lihat Data Galeri<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-4" style="padding: 20px;">
							<i class="icofont icofont-download fa-5x" aria-hidden="true"></i>
						</div>
						<div class="col-lg-8">
							<h3 align="right"><?=$jumlah_file;?> Data</h3>
							<h4 align="right">File</h4>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<a class="tip-top" href="index.php?link=page/file_data.php">Lihat Data File<i class="icofont icofont-double-right pull-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		
	</div>