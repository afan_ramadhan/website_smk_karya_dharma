<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Edit Artikel";
	}
	GantiTitle();
</script>

<?php
$id_artikel = $_GET['id'];
$data_artikel = mysql_query("SELECT * FROM artikel WHERE id_artikel = $id_artikel");
$ambil_data_artikel = mysql_fetch_array($data_artikel);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Artikel</li>
		<li class="active">Edit Artikel</li>
		<li class="active"><?=$ambil_data_artikel['judul'];?></li>
	</ol>
	
	<h3 align="center">Edit Artikel</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/artikel_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="id_kategori">Kategori</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td colspan="3">
					<select class="form-control" name="id_kategori">
						<option disabled="disabled">Pilih Kategori:</option>
						<?php
						$id_kategori = $ambil_data_artikel['id_kategori'];
						$data_kategori = mysql_query("SELECT * FROM kategori_artikel ORDER BY nama_kategori ASC");
						while($ambil_data_kategori = mysql_fetch_array($data_kategori))
						{
						?>
							<option value="<?=$ambil_data_kategori['id_kategori'];?>" <?php if($id_kategori==$ambil_data_kategori['id_kategori']){echo "selected";}?>><?=$ambil_data_kategori['nama_kategori'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
		
			<tr>
				<td>
					<label class="control-label" for="judul">Judul</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="hidden" name="id_artikel" value="<?=$ambil_data_artikel['id_artikel'];?>"/>
					<input type="text" class="form-control" name="judul" value="<?=$ambil_data_artikel['judul'];?>" required>
				</td>
			</tr>

			<tr>
				<td>
					<label class="control-label" for="isi">Isi</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<textarea name="isi"><?=$ambil_data_artikel['isi'];?></textarea>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="upload_gambar">Gambar</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar" name="upload_gambar" onchange="PreviewImage();">
					<span class="help-block">Ukuran gambar maksimal 2 MB, dan gambar harus berformat .jpg</span>
					<div class="bingkai_preview_gambar">
						<?php
						if(empty($ambil_data_artikel['gambar']))
						{
							echo "
								<img id='uploadPreview' class='img-responsive'>";
						}
						else
						{
							echo "
								<img id='uploadPreview' class='img-responsive' src='../gambar/upload/optimized_".$ambil_data_artikel['gambar']."'/>";
						}
						?>
					</div>
				</td>
			</tr>
				
			<tr>
				<td>
					<label class="control-label" for="keterangan_gambar">Ket. Gambar</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="keterangan_gambar" value="<?=$ambil_data_artikel['keterangan_gambar'];?>">
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_artikel"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>