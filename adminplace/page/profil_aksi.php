<?php
if(isset($_POST['simpan_profil']))
{
	include "../konfig/fungsi_upload.php";

	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 
	
	$isi = mysql_real_escape_string($_POST['isi']);

	if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			UploadProfil($nama_file_unik);
			mysql_query("INSERT INTO profil(judul, isi, gambar, keterangan_gambar) VALUES('$_POST[judul]', '$isi', '$nama_file_unik', '$_POST[keterangan_gambar]')");
			echo "
				<script language='javascript'>
					alert('Berhasil Menambahkan!');
				</script>";
		}
		else
		{
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		mysql_query("INSERT INTO profil(judul, isi) VALUES('$_POST[judul]', '$isi')");
		echo "
		<script language='javascript'>
			alert('Berhasil Menambahkan!');
		</script>";
	}
}

if(isset($_POST['hapus_profil']))
{
	$id_profil = $_POST['hapus_profil'];
	$ambil_data = mysql_query("SELECT * FROM profil WHERE id_profil = $id_profil");
	$lihat_data = mysql_fetch_array($ambil_data);

	if(!empty($lihat_data['gambar']))
	{
		unlink("../gambar/data/" . $lihat_data['gambar']);
		unlink("../gambar/data/optimized_" . $lihat_data['gambar']);
		unlink("../gambar/data/thumb_" . $lihat_data['gambar']);
	}

	$hapus = mysql_query("DELETE FROM profil WHERE id_profil = $id_profil");

	if($hapus)
	{
		echo "
		<script language='javascript'>
			alert('Data Berhasil Dihapus!');
		</script>";
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['hapus_terpilih']))
{
	$id_profil = $_POST['terpilih'];
	$banyaknya = count($id_profil);

	if(isset($_POST['hapus_terpilih']))
	{
		for($i = 0; $i < $banyaknya; $i++)
		{
			$ambil_data = mysql_query("SELECT * FROM profil WHERE id_profil = $id_profil[$i]");
			$lihat_data = mysql_fetch_array($ambil_data);
			
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/data/" . $lihat_data['gambar']);
				unlink("../gambar/data/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/data/thumb_" . $lihat_data['gambar']);
			}

			$hapus = mysql_query("DELETE FROM profil WHERE id_profil = $lihat_data[id_profil]");
		}
		
		if($hapus)
		{
			echo "
			<script language='javascript'>
				alert('Data Berhasil Dihapus!');
			</script>";
		}
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['update_profil']))
{
	include "../konfig/fungsi_upload.php";

	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 
	  
	$isi = mysql_real_escape_string($_POST['isi']);

	if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			$ambil_data = mysql_query("SELECT * FROM profil WHERE id_profil = '$_POST[id_profil]'");
			$lihat_data = mysql_fetch_array($ambil_data);
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/data/" . $lihat_data['gambar']);
				unlink("../gambar/data/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/data/thumb_" . $lihat_data['gambar']);	
			}
			UploadProfil($nama_file_unik);
			mysql_query("UPDATE profil SET judul = '$_POST[judul]', isi = '$isi', gambar = '$nama_file_unik', keterangan_gambar = '$_POST[keterangan_gambar]' WHERE id_profil = '$_POST[id_profil]'");
			echo "
			<script>
				alert('Perubahan Disimpan!');
			</script>";
		}
		else
		{
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		mysql_query("UPDATE profil SET judul = '$_POST[judul]', isi = '$isi', keterangan_gambar = '$_POST[keterangan_gambar]' WHERE id_profil = '$_POST[id_profil]'");
		echo "
		<script>
			alert('Perubahan Disimpan!');
		</script>";
	}
}
?>