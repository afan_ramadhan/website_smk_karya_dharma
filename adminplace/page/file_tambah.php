<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Tambah File";
	}
	GantiTitle();
</script>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">File</li>
		<li class="active">Tambah File</li>
	</ol>
	
	<h3 align="center">Tambahkan File</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/file_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="upload_file">File</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" name="upload_file">
					<span class="help-block">Ukuran file maksimal 2 MB</span>
				</td>
			</tr>
			
			<tr>
				<td>
					<label class="control-label" for="nama">Nama</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="nama" required>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="keterangan">Keterangan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<textarea name="keterangan"></textarea>
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="simpan_file"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>