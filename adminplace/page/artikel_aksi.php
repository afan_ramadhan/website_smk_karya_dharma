<?php
if(isset($_POST['simpan_artikel']))
{
	include "../konfig/fungsi_waktu.php";
	include "../konfig/fungsi_upload.php";
	include "../konfig/fungsi_seo.php";

	$tanggal_sekarang = date("Y-m-d");
	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 
	  
	$judul_seo = seo_title($_POST['judul']);
	$isi = mysql_real_escape_string($_POST['isi']);

	if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			UploadGambar($nama_file_unik);
			mysql_query("INSERT INTO artikel(id_kategori, judul, judul_seo, isi, tanggal_post, tanggal, hari, jam, gambar, keterangan_gambar, author) VALUES('$_POST[id_kategori]', '$_POST[judul]', '$judul_seo', '$isi', '$tanggal_sekarang', '$tanggal $bulan_sekarang $tahun', '$hari_ini', '$jam_sekarang', '$nama_file_unik', '$_POST[keterangan_gambar]', '$_SESSION[nama_lengkap]')");
			echo "
			<script language='javascript'>
				alert('Berhasil Menambahkan!');
			</script>";
		}
		else
		{	
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		mysql_query("INSERT INTO artikel(id_kategori, judul, judul_seo, isi, tanggal_post, tanggal, hari, jam, author) VALUES('$_POST[id_kategori]', '$_POST[judul]', '$judul_seo', '$isi', '$tanggal_sekarang', '$tanggal $bulan_sekarang $tahun', '$hari_ini', '$jam_sekarang', '$_SESSION[nama_lengkap]')");
		echo "
		<script language='javascript'>
			alert('Berhasil Menambahkan!');
		</script>";
	}
}

if(isset($_POST['hapus_artikel']))
{
	$id_artikel = $_POST['hapus_artikel'];
	$ambil_data = mysql_query("SELECT * FROM artikel WHERE id_artikel = $id_artikel");
	$lihat_data = mysql_fetch_array($ambil_data);

	if(!empty($lihat_data['gambar']))
	{
		unlink("../gambar/upload/" . $lihat_data['gambar']);
		unlink("../gambar/upload/optimized_" . $lihat_data['gambar']);
		unlink("../gambar/upload/thumb_" . $lihat_data['gambar']);
	}

	$hapus = mysql_query("DELETE FROM artikel WHERE id_artikel = $id_artikel");

	if($hapus)
	{
		echo "
		<script language='javascript'>
			alert('Data Berhasil Dihapus!');
		</script>";
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['hapus_terpilih']))
{
	$id_artikel = $_POST['terpilih'];
	$banyaknya = count($id_artikel);

	if(isset($_POST['hapus_terpilih']))
	{
		for($i = 0; $i < $banyaknya; $i++)
		{
			$ambil_data = mysql_query("SELECT * FROM artikel WHERE id_artikel = $id_artikel[$i]");
			$lihat_data = mysql_fetch_array($ambil_data);
			
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/upload/" . $lihat_data['gambar']);
				unlink("../gambar/upload/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/upload/thumb_" . $lihat_data['gambar']);
			}

			$hapus = mysql_query("DELETE FROM artikel WHERE id_artikel = $lihat_data[id_artikel]");
		}
		
		if($hapus)
		{
			echo "
			<script language='javascript'>
				alert('Data Berhasil Dihapus!');
			</script>";
		}
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['update_artikel']))
{
	include "../konfig/fungsi_upload.php";
	include "../konfig/fungsi_seo.php";

	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 
	  
	$judul_seo = seo_title($_POST['judul']);
	$isi = mysql_real_escape_string($_POST['isi']);	
	
	if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			$ambil_data = mysql_query("SELECT * FROM artikel WHERE id_artikel = '$_POST[id_artikel]'");
			$lihat_data = mysql_fetch_array($ambil_data);
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/upload/" . $lihat_data['gambar']);
				unlink("../gambar/upload/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/upload/thumb_" . $lihat_data['gambar']);	
			}
			UploadGambar($nama_file_unik);
			mysql_query("UPDATE artikel SET id_kategori = '$_POST[id_kategori]', judul = '$_POST[judul]', judul_seo = '$judul_seo', isi = '$isi', gambar = '$nama_file_unik', keterangan_gambar = '$_POST[keterangan_gambar]' WHERE id_artikel = '$_POST[id_artikel]'");
			echo "
			<script>
				alert('Perubahan Disimpan!');
			</script>";	
		}
		else
		{
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		mysql_query("UPDATE artikel SET id_kategori = '$_POST[id_kategori]', judul = '$_POST[judul]', judul_seo = '$judul_seo', isi = '$isi', keterangan_gambar = '$_POST[keterangan_gambar]' WHERE id_artikel = '$_POST[id_artikel]'");
		echo "
		<script>
			alert('Perubahan Disimpan!');
		</script>";
	}
}
?>