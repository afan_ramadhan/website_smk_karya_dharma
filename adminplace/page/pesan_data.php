<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Pesan";
	}
	GantiTitle();
</script>

<?php
include "pesan_aksi.php";
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Pesan</li>
	</ol>
	
	<h3 align="center">Data Pesan</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/pesan_data.php">
		<div class="scrolling">
			<table class="table table-bordered table-hover data_tanggal">
				<thead>
					<tr>
						<th>Tanggal & Waktu</th>
						<th>Pengirim</th>
						<th>Nomor Pengirim</th>
						<th>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				$ambil_data = mysql_query("SELECT * FROM pesan");
				while($hasil_data = mysql_fetch_array($ambil_data))
				{
				?>

					<tr>
						<td style="vertical-align: middle;"><p><input class="terpilih" type="checkbox" name="terpilih[]" value="<?=$hasil_data['id_pesan'];?>" style="padding-right">&nbsp;&nbsp;<?=$hasil_data['tanggal_dikirim'];?> <?=$hasil_data['jam_dikirim'];?></p></td>
						<td style="vertical-align: middle;"><p><?php if($hasil_data['terbaca']=='N'){echo "<span class='label label-warning'>!</span>&nbsp;&nbsp;";}?><?=$hasil_data['nama_pengirim'];?></p></td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['nomor_pengirim'];?></p></td>
						<td align="center" style="vertical-align: middle;">
							<a class="btn btn-sm btn-success tip-button" data-toggle="tooltip" data-original-title="Lihat Pesan" href="index.php?link=page/pesan_tampil.php&id=<?=$hasil_data['id_pesan'];?>"><span class="glyphicon glyphicon-eye-open"></span></a>
							<button type="submit" class="btn btn-sm btn-danger tip-button" data-toggle="tooltip" data-original-title="Hapus"  name="hapus_pesan" value="<?=$hasil_data['id_pesan'];?>" onclick="return confirm('Yakin Hapus Data?');"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				
				<?php
				}
				?>
				</tbody>
			</table>
			<br/>
		</div>
	
		<hr/>
		
		<p align="center">
			<button type="submit" id="hapus_terpilih" class="btn btn-danger" name="hapus_terpilih" onclick="return confirm('Yakin Hapus Data Yang Terpilih?');" style="display: none;">
				<span class="glyphicon glyphicon-trash" style="padding-right: 10px;"></span>Hapus <span id="hitung_terpilih"></span>
			</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>
	
</div>