<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$ambil_data_galeri_konfigurasi['nama_sekolah'];?> | Edit Galeri";
	}
	GantiTitle();
</script>

<?php
$id_galeri = $_GET['id'];
$data_galeri = mysql_query("SELECT * FROM galeri WHERE id_galeri = $id_galeri");
$ambil_data_galeri = mysql_fetch_array($data_galeri);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Galeri</li>
		<li class="active">Edit Galeri</li>
		<li class="active"><?=$ambil_data_galeri['nama'];?></li>
	</ol>
	
	<h3 align="center">Edit Galeri</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/galeri_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="upload_gambar">Ganti</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar" name="upload_gambar" onchange="PreviewImage();"/>
					<span class="help-block">Ukuran gambar maksimal 2 MB, dan gambar harus berformat .jpg</span>
					<div class="tempat_foto_1">
						<img id="uploadPreview" class="img-responsive" style="border: none;" src="../gambar/galeri/optimized_<?=$ambil_data_galeri['gambar'];?>"/>
					</div>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="nama">Nama</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="hidden" name="id_galeri" value="<?=$ambil_data_galeri['id_galeri'];?>">
					<input type="text" class="form-control" name="nama" value="<?=$ambil_data_galeri['nama'];?>" required>
				</td>
			</tr>
			
			<tr>
				<td>
					<label class="control-label" for="keterangan">Keterangan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="keterangan" value="<?=$ambil_data_galeri['keterangan'];?>" required>
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_galeri"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>