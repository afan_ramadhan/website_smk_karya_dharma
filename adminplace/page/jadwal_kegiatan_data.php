<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Data Jadwal Kegiatan";
	}
	GantiTitle();
</script>

<?php
include "jadwal_kegiatan_aksi.php";
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Jadwal Kegiatan</li>
		<li class="active">Data Jadwal Kegiatan</li>
	</ol>
	
	<h3 align="center">Data Jadwal Kegiatan</h3>
	
	<br/>
	
	<p><a class="btn btn-default" href="index.php?link=page/jadwal_kegiatan_tambah.php"><i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Tambah Jadwal Kegiatan</a></p>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/jadwal_kegiatan_data.php">
		<div class="scrolling">
			<table class="table table-bordered table-hover data_tanggal">
				<thead>
					<tr>
						<th>Tanggal & Waktu</th>
						<th>Kegiatan</th>
						<th>Waktu Kegiatan</th>
						<th>Tempat</th>
						<th>Penulis</th>
						<th>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				$ambil_data = mysql_query("SELECT * FROM jadwal_kegiatan");
				while($hasil_data = mysql_fetch_array($ambil_data))
				{
				?>

					<tr>
						<td style="vertical-align: middle;"><p><input class="terpilih" type="checkbox" name="terpilih[]" value="<?=$hasil_data['id_kegiatan'];?>" style="padding-right">&nbsp;&nbsp;<?=$hasil_data['tanggal_post'];?> <?=$hasil_data['jam'];?></p></td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['nama_kegiatan'];?></p></td>
						<td style="vertical-align: middle;">
							<p><?=$hasil_data['tanggal_kegiatan_mulai'];?><?php if(!empty($hasil_data['tanggal_kegiatan_selesai'])){echo " Sampai $hasil_data[tanggal_kegiatan_selesai]";}?></p>
							<p><?=$hasil_data['jam_kegiatan_mulai'];?><?php if(!empty($hasil_data['jam_kegiatan_selesai'])){echo " Sampai $hasil_data[jam_kegiatan_selesai]";}?></p>
						</td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['tempat'];?></p></td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['author'];?></p></td>
						<td align="center" style="vertical-align: middle;">
							<a class="btn btn-sm btn-warning tip-button" data-toggle="tooltip" data-original-title="Edit" href="index.php?link=page/jadwal_kegiatan_edit.php&id=<?=$hasil_data['id_kegiatan'];?>"><span class="glyphicon glyphicon-edit"></span></a>
							<button type="submit" class="btn btn-sm btn-danger tip-button" data-toggle="tooltip" data-original-title="Hapus"  name="hapus_jadwal_kegiatan" value="<?=$hasil_data['id_kegiatan'];?>" onclick="return confirm('Yakin Hapus Data?');"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				
				<?php
				}
				?>
				</tbody>
			</table>
			<br/>
		</div>
	
		<hr/>
		
		<p align="center">
			<button type="submit" id="hapus_terpilih" class="btn btn-danger" name="hapus_terpilih" onclick="return confirm('Yakin Hapus Data Yang Terpilih?');" style="display: none;">
				<span class="glyphicon glyphicon-trash" style="padding-right: 10px;"></span>Hapus <span id="hitung_terpilih"></span>
			</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>
	
</div>