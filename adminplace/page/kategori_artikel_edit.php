<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Edit Kategori Artikel";
	}
	GantiTitle();
</script>

<?php
$id_kategori = $_GET['id'];
$data_kategori_artikel = mysql_query("SELECT * FROM kategori_artikel WHERE id_kategori = $id_kategori");
$ambil_data_kategori_artikel = mysql_fetch_array($data_kategori_artikel);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Kategori Artikel</li>
		<li class="active">Edit Kategori Artikel</li>
		<li class="active"><?=$ambil_data_kategori_artikel['nama_kategori'];?></li>
	</ol>
	
	<h3 align="center">Edit Kategori Artikel</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/kategori_artikel_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="nama_kategori">Nama Kategori</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="hidden" name="id_kategori" value="<?=$ambil_data_kategori_artikel['id_kategori'];?>"/>
					<input type="text" class="form-control" name="nama_kategori" value="<?=$ambil_data_kategori_artikel['nama_kategori'];?>" maxlength="50" required>
				</td>
			</tr>

			<tr>
				<td>
					<label class="control-label" for="keterangan">Keterangan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<textarea name="keterangan"><?=$ambil_data_kategori_artikel['keterangan'];?></textarea>
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_kategori_artikel"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>