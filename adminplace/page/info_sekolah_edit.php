<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Edit Info Sekolah";
	}
	GantiTitle();
</script>

<?php
$id_info_sekolah = $_GET['id'];
$data_info_sekolah = mysql_query("SELECT * FROM info_sekolah WHERE id_info_sekolah = $id_info_sekolah");
$ambil_data_info_sekolah = mysql_fetch_array($data_info_sekolah);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Info Sekolah</li>
		<li class="active">Edit Info Sekolah</li>
		<li class="active"><?=$ambil_data_info_sekolah['judul'];?></li>
	</ol>
	
	<h3 align="center">Edit Info Sekolah</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/info_sekolah_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="judul">Judul</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="hidden" name="id_info_sekolah" value="<?=$ambil_data_info_sekolah['id_info_sekolah'];?>"/>
					<input type="text" class="form-control" name="judul" value="<?=$ambil_data_info_sekolah['judul'];?>" required>
				</td>
			</tr>

			<tr>
				<td>
					<label class="control-label" for="isi">Isi</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<textarea name="isi"><?=$ambil_data_info_sekolah['isi'];?></textarea>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="upload_gambar">Gambar</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar" name="upload_gambar" onchange="PreviewImage();">
					<span class="help-block">Ukuran gambar maksimal 2 MB, dan gambar harus berformat .jpg</span>
					<div class="bingkai_preview_gambar">
						<?php
						if(empty($ambil_data_info_sekolah['gambar']))
						{
							echo "
								<img id='uploadPreview' class='img-responsive'>";
						}
						else
						{
							echo "
								<img id='uploadPreview' class='img-responsive' src='../gambar/upload/optimized_".$ambil_data_info_sekolah['gambar']."'/>";
						}
						?>
					</div>
				</td>
			</tr>
				
			<tr>
				<td>
					<label class="control-label" for="keterangan_gambar">Ket. Gambar</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="keterangan_gambar" value="<?=$ambil_data_info_sekolah['keterangan_gambar'];?>">
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_info_sekolah"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>