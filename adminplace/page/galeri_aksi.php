<?php
if(isset($_POST['simpan_galeri']))
{
	include "../konfig/fungsi_waktu.php";
	include "../konfig/fungsi_upload.php";

	$tanggal_sekarang = date("Y-m-d");
	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 
	  
	 if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			UploadGaleri($nama_file_unik);
			mysql_query("INSERT INTO galeri(gambar, nama, keterangan, tanggal_post, tanggal, hari, jam, author) VALUES('$nama_file_unik', '$_POST[nama]', '$_POST[keterangan]', '$tanggal_sekarang', '$tanggal $bulan_sekarang $tahun', '$hari_ini', '$jam_sekarang', '$_SESSION[nama_lengkap]')");
			echo "
				<script language='javascript'>
					alert('Berhasil Menambahkan!');
				</script>";
		}
		else
		{
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		echo "
		<script language='javascript'>
			alert('Anda Belum Memilih Gambar!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['hapus_galeri']))
{
	$id_galeri = $_POST['hapus_galeri'];
	$ambil_data = mysql_query("SELECT * FROM galeri WHERE id_galeri = $id_galeri");
	$lihat_data = mysql_fetch_array($ambil_data);

	if(!empty($lihat_data['gambar']))
	{
		unlink("../gambar/galeri/" . $lihat_data['gambar']);
		unlink("../gambar/galeri/optimized_" . $lihat_data['gambar']);
		unlink("../gambar/galeri/thumb_" . $lihat_data['gambar']);
	}

	$hapus = mysql_query("DELETE FROM galeri WHERE id_galeri = $id_galeri");

	if($hapus)
	{
		echo "
		<script language='javascript'>
			alert('Data Berhasil Dihapus!');
		</script>";
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['hapus_terpilih']))
{
	$id_galeri = $_POST['terpilih'];
	$banyaknya = count($id_galeri);

	if(isset($_POST['hapus_terpilih']))
	{
		for($i = 0; $i < $banyaknya; $i++)
		{
			$ambil_data = mysql_query("SELECT * FROM galeri WHERE id_galeri = $id_galeri[$i]");
			$lihat_data = mysql_fetch_array($ambil_data);
			
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/galeri/" . $lihat_data['gambar']);
				unlink("../gambar/galeri/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/galeri/thumb_" . $lihat_data['gambar']);
			}

			$hapus = mysql_query("DELETE FROM galeri WHERE id_galeri = $lihat_data[id_galeri]");
		}
		
		if($hapus)
		{
			echo "
			<script language='javascript'>
				alert('Data Berhasil Dihapus!');
			</script>";
		}
	}
	else 
	{
		echo "
		<script language='javascript'>
			alert('Gagal Menghapus!');
			window.history.back();
		</script>";
	}
}

if(isset($_POST['update_galeri']))
{
	include "../konfig/fungsi_upload.php";

	$lokasi_file = $_FILES['upload_gambar']['tmp_name'];
	$tipe_file = $_FILES['upload_gambar']['type'];
	$nama_file = $_FILES['upload_gambar']['name'];
	$acak = rand(1,99);
	$nama_file_unik = $acak.$nama_file; 

	if(!empty($lokasi_file))
	{
		if($tipe_file == "image/jpeg" or $tipe_file == "image/jpg" or $tipe_file == "image/pjpeg")
		{
			$ambil_data = mysql_query("SELECT * FROM galeri WHERE id_galeri = '$_POST[id_galeri]'");
			$lihat_data = mysql_fetch_array($ambil_data);
			if(!empty($lihat_data['gambar']))
			{
				unlink("../gambar/galeri/" . $lihat_data['gambar']);
				unlink("../gambar/galeri/optimized_" . $lihat_data['gambar']);
				unlink("../gambar/galeri/thumb_" . $lihat_data['gambar']);	
			}
			UploadGaleri($nama_file_unik);
			mysql_query("UPDATE galeri SET gambar = '$nama_file_unik', nama = '$_POST[nama]', keterangan = '$_POST[keterangan]' WHERE id_galeri = '$_POST[id_galeri]'");
			echo "
				<script>
					alert('Perubahan Disimpan!');
				</script>";
		}
		else
		{
			echo "
			<script>
				alert('Gambar Harus Berformat .jpg!');
				window.history.back();
			</script>";
		}
	}
	else
	{
		mysql_query("UPDATE galeri SET nama = '$_POST[nama]', keterangan = '$_POST[keterangan]' WHERE id_galeri = '$_POST[id_galeri]'");
		echo "
		<script>
			alert('Perubahan Disimpan!');
		</script>";
	}
}
?>