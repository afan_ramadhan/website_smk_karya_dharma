<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$ambil_data_file_konfigurasi['nama_sekolah'];?> | Edit File";
	}
	GantiTitle();
</script>

<?php
$id_file = $_GET['id'];
$data_file = mysql_query("SELECT * FROM file WHERE id_file = $id_file");
$ambil_data_file = mysql_fetch_array($data_file);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">File</li>
		<li class="active">Edit File</li>
		<li class="active"><?=$ambil_data_file['nama'];?></li>
	</ol>
	
	<h3 align="center">Edit File</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/file_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="upload_file">Ganti</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" name="upload_file"/>
					<span class="help-block">Ukuran file maksimal 2 MB</span>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="nama">Nama</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="hidden" name="id_file" value="<?=$ambil_data_file['id_file'];?>">
					<input type="text" class="form-control" name="nama" value="<?=$ambil_data_file['nama'];?>" required>
				</td>
			</tr>
			
			<tr>
				<td>
					<label class="control-label" for="keterangan">Keterangan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<textarea name="keterangan"><?=$ambil_data_file['keterangan'];?></textarea>
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_file"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>