<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Tambah Jadwal Kegiatan";
	}
	GantiTitle();
</script>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Jadwal Kegiatan</li>
		<li class="active">Tambah Jadwal Kegiatan</li>
	</ol>
	
	<h3 align="center">Tambahkan Jadwal Kegiatan</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/jadwal_kegiatan_data.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="nama_kegiatan">Kegiatan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td colspan="3">
					<input type="text" class="form-control" name="nama_kegiatan" required>
				</td>
			</tr>

			<tr>
				<td>
					<label class="control-label" for="keterangan">Keterangan</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td  colspan="3">
					<textarea name="keterangan"></textarea>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="tanggal_kegiatan_mulai">Tanggal</label>
				</td>
				<td><label class="control-label">:</label></td>				
				<td>
					<div class='input-group date' id='datepicker1'>
						<input type='text' class="form-control" name="tanggal_kegiatan_mulai" required/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</td>
				
				<td align="center">
					<label class="control-label" for="tanggal_kegiatan_selesai">Sampai</label>
				</td>	
				<td>
					<div class='input-group date' id='datepicker2'>
						<input type='text' class="form-control" name="tanggal_kegiatan_selesai"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="jam_kegiatan_mulai">Pukul</label>
				</td>
				<td><label class="control-label">:</label></td>				
				<td>
					<div class='input-group date' id='timepicker1'>
						<input type='text' class="form-control" name="jam_kegiatan_mulai" required/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-time"></span>
						</span>
					</div>
				</td>
				<td align="center">
					<label class="control-label" for="jam_kegiatan_selesai">Sampai</label>
				</td>
				<td>
					<div class='input-group date' id='timepicker2'>
						<input type='text' class="form-control" name="jam_kegiatan_selesai"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-time"></span>
						</span>
					</div>
				</td>
			</tr>
  
			<tr>
				<td>
					<label class="control-label" for="tempat">Tempat</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td colspan="3">
					<input type="text" class="form-control" name="tempat" required>
				</td>
			</tr>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="simpan_jadwal_kegiatan"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>