<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_user['nama_sekolah'];?> | Pengaturan Admin";
	}
	GantiTitle();
</script>

<?php
include "user_aksi.php";
$data_user = mysql_query("SELECT * FROM user WHERE id_user = '$_SESSION[id_user]'");
$ambil_data_user = mysql_fetch_array($data_user);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Pengaturan Admin</li>
	</ol>
	
	<h3 align="center">Pengaturan Admin</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/user_edit.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<input type="hidden" name="id_user" value="<?=$ambil_data_user['id_user'];?>">
					<label class="control-label" for="nama_lengkap">Nama Lengkap</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="nama_lengkap" value="<?=$ambil_data_user['nama_lengkap'];?>" maxlength="75" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="email">email</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="email" value="<?=$ambil_data_user['email'];?>" maxlength="100">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="nomor_telepon">Nomor Telepon</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="nomor_telepon" value="<?=$ambil_data_user['nomor_telepon'];?>" maxlength="15">
				</td>
			</tr>	
			<tr>
				<td>
					<label class="control-label" for="username">Username</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="username" value="<?=$ambil_data_user['username'];?>" maxlength="50" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="password_baru">Password Baru</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="password" class="form-control tanpa_spasi" name="password_baru" maxlength="50">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="ulangi_password">Ulangi&nbsp;Password&nbsp;Baru</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="password" class="form-control tanpa_spasi" name="ulangi_password" maxlength="50">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="upload_gambar">Gambar</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar" name="upload_gambar" onchange="PreviewImage();">
					<span class="help-block">Ukuran gambar maksimal 2 MB, dan gambar harus berformat .jpg</span>
					<div class="tempat_foto_1">
						<?php
						if(empty($ambil_data_user['gambar']))
						{
							echo "
								<img id='uploadPreview' class='img-responsive' style='border: none;'>";
						}
						else
						{
							echo "
								<img id='uploadPreview' class='img-responsive' style='border: none;' src='../gambar/user/optimized_".$ambil_data_user['gambar']."'/>";
						}
						?>
					</div>
				</td>
			</tr>
				
		</table>

		<hr/>
		
		<p align="center">
			<a class="btn btn-success" data-toggle="modal" data-target="#verifikasi_password"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</a>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>
		
		<div class="modal fade" id="verifikasi_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="top: 30%;">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Masukan Password</h4>
					</div>
					<div class="modal-body">
						<table class="table" style="border: 0px;">
							<tr>
								<td style="border: none">
									<label class="control-label" for="password_lama">Password Lama</label>
								</td>
								<td style="border: none">
									<label class="control-label"style="vertical-align: middle;">:</label>
								</td>
								<td style="border: none">
									<input type="password" class="form-control tanpa_spasi" name="password_lama" maxlength="50">
								</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success" name="update_user"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
		
		<script>
			$("input.tanpa_spasi").on({
				keydown: function(e){
				if (e.which === 32)
				return false;
				},
				change: function() {
				this.value = this.value.replace(/\s/g, "");
				}
			});
		</script>

	</form>

</div>