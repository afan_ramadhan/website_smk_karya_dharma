<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Konfigurasi";
	}
	GantiTitle();
</script>

<?php
include "konfigurasi_aksi.php";
$data_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id_konfigurasi = '1'");
$ambil_data_konfigurasi = mysql_fetch_array($data_konfigurasi);
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Konfigurasi</li>
	</ol>
	
	<h3 align="center">Konfigurasi</h3>
	
	<br/>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/konfigurasi_edit.php" enctype="multipart/form-data">
		<table class="table table-hover">
		
			<tr>
				<td>
					<label class="control-label" for="nama_sekolah">Nama&nbsp;Sekolah</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="nama_sekolah" value="<?=$ambil_data_konfigurasi['nama_sekolah'];?>" maxlength="100" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="deskripsi">Deskripsi</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="deskripsi" value="<?=$ambil_data_konfigurasi['deskripsi'];?>" maxlength="100" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="kata_kunci">Kata Kunci</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="kata_kunci" value="<?=$ambil_data_konfigurasi['kata_kunci'];?>" maxlength="100" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="alamat">Alamat</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="alamat" value="<?=$ambil_data_konfigurasi['alamat'];?>" maxlength="100" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="telepon">Telepon</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control" name="telepon" value="<?=$ambil_data_konfigurasi['telepon'];?>" maxlength="15" required>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="email">Email</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="email" value="<?=$ambil_data_konfigurasi['email'];?>" maxlength="100">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="peta">Peta</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="peta" value="<?=$ambil_data_konfigurasi['peta'];?>">
					<span class="help-block">Source peta yang didapat dari Google Maps</span>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="facebook">Facebook</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="facebook" value="<?=$ambil_data_konfigurasi['facebook'];?>" maxlength="100">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="twitter">Twitter</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="twitter" value="<?=$ambil_data_konfigurasi['twitter'];?>" maxlength="100">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="instagram">Instagram</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="text" class="form-control tanpa_spasi" name="instagram" value="<?=$ambil_data_konfigurasi['instagram'];?>" maxlength="100">
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="gambar_header">Header</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar_1" name="gambar_header" onchange="PreviewImage1();">
					<span class="help-block">Ukuran gambar maksimal 2 MB, dengan dimensi yang disarankan 1140 x 120 pixels, dan gambar harus berformat .jpg</span>
					<div class="tempat_foto_1">
						<?php
						if(empty($ambil_data_konfigurasi['gambar_header']))
						{
							echo "
								<img id='uploadPreview1' class='img-responsive'>";
						}
						else
						{
							echo "
								<img id='uploadPreview1' class='img-responsive' src='../gambar/konfigurasi/".$ambil_data_konfigurasi['gambar_header']."'/>";
						}
						?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="background">Background</label>
				</td>
				<td><label class="control-label">:</label></td>
				<td>
					<input type="file" id="input_gambar_2" name="background" onchange="PreviewImage2();">
					<span class="help-block">Ukuran gambar maksimal 2 MB, dan gambar harus berformat .jpg</span>
					<div class="tempat_foto_1">
						<?php
						if(empty($ambil_data_konfigurasi['background']))
						{
							echo "
								<img id='uploadPreview2' class='img-responsive'>";
						}
						else
						{
							echo "
								<img id='uploadPreview2' class='img-responsive' src='../gambar/konfigurasi/".$ambil_data_konfigurasi['background']."'/>";
						}
						?>
					</div>
				</td>
			</tr>
			
			<script>
				$("input.tanpa_spasi").on({
					keydown: function(e){
					if (e.which === 32)
					return false;
					},
					change: function(){
					this.value = this.value.replace(/\s/g, "");
					}
				});
			</script>
				
		</table>

		<hr/>
			
		<p align="center">
			<button type="reset" class="btn btn-danger"><i class="fa fa-refresh fa-lg" aria-hidden="true"></i></button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-success" name="update_konfigurasi"><i class="fa fa-check fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Simpan</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>

</div>