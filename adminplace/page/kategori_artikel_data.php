<script language="JavaScript">
	function GantiTitle(){
		document.title="Admin <?=$lihat_konfigurasi['nama_sekolah'];?> | Data Kategori Artikel";
	}
	GantiTitle();
</script>

<?php
include "kategori_artikel_aksi.php";
?>

<div class="konten_admin">

	<ol class='breadcrumb'>
		<li><a class="tip-top" href="index.php">Beranda Admin</a></li>
		<li class="active">Kategori Artikel</li>
		<li class="active">Data Kategori Artikel</li>
	</ol>
	
	<h3 align="center">Data Kategori Artikel</h3>
	
	<br/>
	
	<p><a class="btn btn-default" href="index.php?link=page/kategori_artikel_tambah.php"><i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Tambah Kategori Artikel</a></p>
	
	<form class="form-horizontal" role="form" method="post" action="index.php?link=page/kategori_artikel_data.php">
		<div class="scrolling">
			<table class="table table-bordered table-hover data">
				<thead>
					<tr>
						<th>Nama Kategori</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
				</thead>
				
				<tbody>
				<?php
				$ambil_data = mysql_query("SELECT * FROM kategori_artikel");
				while($hasil_data = mysql_fetch_array($ambil_data))
				{
				?>

					<tr>
						<td style="vertical-align: middle;"><p><input class="terpilih" type="checkbox" name="terpilih[]" value="<?=$hasil_data['id_kategori'];?>" style="padding-right">&nbsp;&nbsp;<?=$hasil_data['nama_kategori'];?></p></td>
						<td style="vertical-align: middle;"><p><?=$hasil_data['keterangan'];?></p></td>
						<td align="center" style="vertical-align: middle;">
							<a class="btn btn-sm btn-warning tip-button" data-toggle="tooltip" data-original-title="Edit" href="index.php?link=page/kategori_artikel_edit.php&id=<?=$hasil_data['id_kategori'];?>"><span class="glyphicon glyphicon-edit"></span></a>
							<button type="submit" class="btn btn-sm btn-danger tip-button" data-toggle="tooltip" data-original-title="Hapus"  name="hapus_kategori_artikel" value="<?=$hasil_data['id_kategori'];?>" onclick="return confirm('Yakin Hapus Data?');"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				
				<?php
				}
				?>
				</tbody>
			</table>
			<br/>
		</div>
	
		<hr/>
		
		<p align="center">
			<button type="submit" id="hapus_terpilih" class="btn btn-danger" name="hapus_terpilih" onclick="return confirm('Yakin Hapus Data Yang Terpilih?');" style="display: none;">
				<span class="glyphicon glyphicon-trash" style="padding-right: 10px;"></span>Hapus <span id="hitung_terpilih"></span>
			</button>
			&nbsp;&nbsp;
			<a class="btn btn-primary" href="index.php"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 10px;"></i>Beranda Admin</a>
		</p>

	</form>
	
</div>