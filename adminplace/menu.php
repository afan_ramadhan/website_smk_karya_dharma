<div class="panel panel-default">

	<div class="panel-heading">
		<br/>
		<p align="center">
			<img class="img-thumbnail" src="../gambar/user/thumb_<?=$ambil_data_adminweb['gambar'];?>" style="width: 100px;"/>
		</p>
		<p align="center" class="bold">
			<?php
			$nama = explode(" ",$ambil_data_adminweb['nama_lengkap']);
			echo "Hai, $nama[0]";
			?>
			<i class="icofont icofont-emo-simple-smile" aria-hidden="true" style="padding-left: 10px;"></i>
		</p>	
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" data-toggle="collapse" href="#list_1"><i class="icofont icofont-building-alt" aria-hidden="true" style="padding-right: 10px;"></i>Profil<i class="fa fa-caret-down" aria-hidden="true" style="float: right;"></i></a>
		</h4>
	</div>
	<div id="list_1" class="panel-collapse collapse" style="border-bottom: 1px solid #DDDDDD">
		<ul class="list-group">
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/profil_tambah.php">Tambah Profil Sekolah</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/profil_data.php">Data Profil Sekolah</a></li>
		</ul>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" data-toggle="collapse" href="#list_2"><i class="icofont icofont-paper" aria-hidden="true" style="padding-right: 10px;"></i>Info Sekolah & Artikel<i class="fa fa-caret-down" aria-hidden="true" style="float: right;"></i></a>
		</h4>
	</div>
	<div id="list_2" class="panel-collapse collapse" style="border-bottom: 1px solid #DDDDDD">
		<ul class="list-group">
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/info_sekolah_tambah.php">Tambah Info Sekolah</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/info_sekolah_data.php">Data Info Sekolah</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/kategori_artikel_tambah.php">Tambah Kategori Artikel</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/kategori_artikel_data.php">Data Kategori Artikel</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/artikel_tambah.php">Tambah Artikel</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/artikel_data.php">Data Artikel</a></li>
		</ul>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" data-toggle="collapse" href="#list_3"><i class="icofont icofont-tasks-alt" aria-hidden="true" style="padding-right: 10px;"></i>Jadwal Kegiatan<i class="fa fa-caret-down" aria-hidden="true" style="float: right;"></i></a>
		</h4>
	</div>
	<div id="list_3" class="panel-collapse collapse" style="border-bottom: 1px solid #DDDDDD">
		<ul class="list-group">
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/jadwal_kegiatan_tambah.php">Tambah Jadwal Kegiatan</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/jadwal_kegiatan_data.php">Data Jadwal Kegiatan</a></li>
		</ul>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" data-toggle="collapse" href="#list_8"><i class="icofont icofont-image" aria-hidden="true" style="padding-right: 10px;"></i>Galeri<i class="fa fa-caret-down" aria-hidden="true" style="float: right;"></i></a>
		</h4>
	</div>
	<div id="list_8" class="panel-collapse collapse" style="border-bottom: 1px solid #DDDDDD">
		<ul class="list-group">
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/galeri_tambah.php">Tambahkan Foto Atau Gambar</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/galeri_data.php">Data Galeri</a></li>
		</ul>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" data-toggle="collapse" href="#list_9"><i class="icofont icofont-download" aria-hidden="true" style="padding-right: 10px;"></i>File<i class="fa fa-caret-down" aria-hidden="true" style="float: right;"></i></a>
		</h4>
	</div>
	<div id="list_9" class="panel-collapse collapse" style="border-bottom: 1px solid #DDDDDD">
		<ul class="list-group">
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/file_tambah.php">Tambahkan File</a></li>
			<li class="list-group-item"><a class="no_decoration" style="padding-left: 25px;" href="index.php?link=page/file_data.php">Data File</a></li>
		</ul>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<h4 class="panel-title">
			<a class="no_decoration" href="index.php?link=page/konfigurasi_edit.php"><i class="icofont icofont-hammer" aria-hidden="true" style="padding-right: 10px;"></i>Konfigurasi</a>
		</h4>
	</div>
	
	<div class="panel-body" style="border-bottom: 1px solid #DDDDDD">
		<center>
			<?php
			$tanggal = date("Ymd");
			$pengunjung = mysql_num_rows(mysql_query("SELECT * FROM statistics WHERE tanggal = '$tanggal' GROUP BY ip"));
			$totalpengunjung = mysql_result(mysql_query("SELECT COUNT(hits) FROM statistics"), 0); 
			$hits = mysql_fetch_assoc(mysql_query("SELECT SUM(hits) AS hitstoday FROM statistics WHERE tanggal = '$tanggal' GROUP BY tanggal")); 
			$totalhits = mysql_result(mysql_query("SELECT SUM(hits) FROM statistics"), 0); 
			$tothitsgbr = mysql_result(mysql_query("SELECT SUM(hits) FROM statistics"), 0); 
			$bataswaktu = time() - 300;
			$pengunjungonline = mysql_num_rows(mysql_query("SELECT * FROM statistics WHERE online > '$bataswaktu'"));

			$path = "../gambar/statistics/";
			$ext = ".png";

			$tothitsgbr = sprintf("%06d", $tothitsgbr);
			for ($i = 0; $i <= 9; $i++)
			{
				$tothitsgbr = str_replace($i, "<img src='$path$i$ext' alt='$i'> ", $tothitsgbr);
			}

			echo "
			<table>
				<tr>
					<td align='center' style='padding: 10px;'>$tothitsgbr</td>
				</tr>
				<tr>
					<td style='padding: 3px;'><img src='../gambar/statistics/hariini.png' style='padding-right: 10px;'/>Pengunjung hari ini : $pengunjung</td>
				</tr>
				<tr>
					<td style='padding: 3px;'><img src='../gambar/statistics/total.png' style='padding-right: 10px;'/>Total pengunjung : $totalpengunjung</td>
				</tr>
				<tr>
					<td style='padding: 3px;'><img src='../gambar/statistics/hariini.png' style='padding-right: 10px;'/>Hits hari ini : $hits[hitstoday]</td>
				</tr>
				<tr>
					<td style='padding: 3px;'><img src='../gambar/statistics/total.png' style='padding-right: 10px;'/>Total Hits : $totalhits</td>
				</tr>
				<tr>
					<td style='padding: 3px;'><img src='../gambar/statistics/online.png' style='padding-right: 10px;'/>Pengunjung Online : $pengunjungonline</td>
				</tr>
			</table>";
			?>
		</center>
	</div>

</div>