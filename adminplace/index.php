<?php
ob_start("ob_gzhandler");
session_start();
include "../konfig/koneksi.php";

if(empty($_SESSION['username']) and empty($_SESSION['level']))
{
	header("location:login.php");
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id_konfigurasi = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);
?>

<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title>Admin <?=$lihat_konfigurasi['nama_sekolah'];?></title>
		
		<link href="../gambar/favicon_admin.ico" type="image/x-icon" rel="shortcut icon"/>
		<link href="../script/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../script/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		<link href="../script/ghuroba_script/ghuroba.css" rel="stylesheet"/>
		<link href="../script/calendar/css/pickmeup.css" rel="stylesheet" type="text/css"/>
		<link href="../script/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
		<link href="../script/icofont/css/icofont.css" rel="stylesheet"/>
		<link href="../script/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet"/>
		<link href="../script/datatables/media/css/dataTables.bootstrap.css" type="text/css" rel="stylesheet"/>
		
	</head>

	<body class="background_admin">

		<script src="../script/jquery/jquery-1.12.3.min.js"></script>
		
		<script src="../script/ghuroba_script/ghuroba.js"></script>
		
		<script src="../script/calendar/js/jquery.pickmeup.js" type="text/javascript"></script>
		<script src="../script/calendar/js/demo.js" type="text/javascript"></script>
		
		<script src="../script/tinymce/tinymce.min.js" type="text/javascript"></script>
		<script>
			tinymce.init({
				selector: 'textarea',
				height: 350,
				theme: 'modern',
				plugins: [
				'advlist autolink lists link image charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code fullscreen',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'template paste textcolor colorpicker textpattern imagetools'
				],
				toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
				toolbar2: 'print preview media | forecolor backcolor emoticons'
			});
		</script>
		
		<script src="../script/bootstrap/js/bootstrap.min.js"></script>
		<script src="../script/bootstrap/js/moment.js"></script>
		<script src="../script/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript">
			$(function () {
				$('#datetimepicker').datetimepicker({
					format: 'DD MMMM YYYY HH:mm',
				});
				$('#datepicker1').datetimepicker({
					format: 'DD/MM/YYYY',
				});
				$('#datepicker2').datetimepicker({
					format: 'DD/MM/YYYY',
				});
				$('#timepicker1').datetimepicker({
					format: 'HH:mm'
				});
				$('#timepicker2').datetimepicker({
					format: 'HH:mm'
				});
			});
		</script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$(".tip-top").tooltip({
					placement : 'top'
				});
				$(".tip-right").tooltip({
					placement : 'right'
				});
				$(".tip-bottom").tooltip({
					placement : 'bottom'
				});
				$(".tip-left").tooltip({
					placement : 'left'
				});
				$(".tip-button").tooltip({
					placement : 'top'
				});
			});
		</script>
		
		<script src="../script/datatables/media/js/jquery.dataTables.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				$('.data').DataTable();
				$('.data_tanggal').DataTable({
					aaSorting: [[0, 'desc']]
				});
			});
		</script>
		
		<script src="../script/highcharts/highcharts.js" type="text/javascript"></script>
		
	<!---------------------------------------------------------------->	

		<?php
		if(isset($_SESSION['username']) and $_SESSION['level'] == "Admin")
		{
			include "header.php";
			include "content.php";
		}
		else
		{
			echo "
			<script language='javascript'>
				alert('Sesi Telah Habis!');
				window.location.href='logout.php';
			</script>";
		}
		?>
		
	<!---------------------------------------------------------------->

	</body>

</html>