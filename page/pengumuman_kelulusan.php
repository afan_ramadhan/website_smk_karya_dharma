<script language="JavaScript">
	function GantiTitle(){
		document.title="<?=$ambil_data_konfigurasi['nama_sekolah'];?> | Pengumumnam Kelulusan";
	}
	GantiTitle();
</script>

<br/>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="rb-wrap">
			<h3 class="ribbon">
				<strong class="ribbon-content">Pengumuman Kelulusan</strong>
			</h3>
		</div>
		<br/>
		
		<center>
			<table>
				<tr>
					<td style="padding: 5px;"><label for="nomor_peserta" class="control-label">Nomor Peserta :</label></td>
					<td style="padding: 5px;"><input type="text" class="form-control" id="nomor_peserta" placeholder="Nomor Peserta Ujian"></td>
					<td style="padding: 5px;"><a class="btn btn-default" href="#result-box" onclick="cekKelulusan()">Submit</a></td>
				</tr>
			</table>
		</center>
		
		<div id="result-box" style="margin-top: 50px; margin-bottom: 100px;">
		</div>
			
	</div>
</div>

<div class="modal fade" id="sambutan" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title custom-font-1">Assalamu'alaikum Warohmatullahi Wabarokatuh.. .</h4>
			</div>
			<div class="modal-body">
				<p>Selamat atas kelulusanmu. Semoga kamu akan terus sukses dan sukses di kehidupan yang akan datang. 😉</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onclick="playAudio()">Tutup</button>
			</div>
		</div>
	</div>
</div>
		
<audio id="opening" controls style="display: none;">
	<source src="file/alhamdulillah.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
</audio>

<script>
	function cekKelulusan()
	{
		var mod = "cekKelulusan";
		var nomor_peserta = $("#nomor_peserta").val();
		
		if(nomor_peserta == "")
		{
			alert("Nomor Peserta Wajib Diisi!");
		}
		else
		{
			$.ajax({
				type	: "POST",
				url		: "page/pengumuman_kelulusan_response.php",
				data	: "mod=" + mod +
						  "&nomor_peserta=" + nomor_peserta,
				success: function(response)
				{
					$("#result-box").empty(response);
					$("#result-box").append(response);
				}
			})
		}
	}
	
	// $("#sambutan").modal();
	
	// var audio = document.getElementById("opening"); 
			
	// function playAudio(){
		// audio.play();
	// }
	
	// $('#sambutan').on('hidden.bs.modal', function(){
		// playAudio();
	// })
</script>