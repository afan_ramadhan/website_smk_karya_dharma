<script language="JavaScript">
	function GantiTitle(){
		document.title="<?=$ambil_data_konfigurasi['nama_sekolah'];?> | Kontak";
	}
	GantiTitle();
</script>

<?php
include "pesan_simpan.php";
?>

<br/>

<?php
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
{
?>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="rb-wrap">
				<h3 class="ribbon">
					<strong class="ribbon-content">Kontak</strong>
				</h3>
			</div>
			<br/>
			<table class="table table-hover">
				<tr>
					<th width="20%">Telepon</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['telepon'];?></td>
				</tr>
				<tr>
					<th>Email</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['email'];?></td>
				</tr>
				<tr>
					<th>Alamat</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['alamat'];?></td>
				</tr>
				<tr>
					<th>Facebook</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['facebook'];?></td>
				</tr>
				<tr>
					<th>Instagram</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['instagram'];?></td>
				</tr>
				<tr>
					<th>Twitter</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['twitter'];?></td>
				</tr>
			</table>

		</div>
	</div>

<?php
}
else
{
?>

	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<div class="panel panel-primary shadow" style="border: none;">
				<div class="panel-heading" style="background-color: #4F8EC7; border: none;"><h4><b>Profil</b><i class="icofont icofont-building-alt fa-lg" aria-hidden="true" style="padding-left: 15px;"></i></h4></div>
				<div class="panel-body" style="background-color: #FFFFF;">
					<?php
					$profil = mysql_num_rows(mysql_query("SELECT * FROM profil"));

					if($profil==0)
					{
						echo "<center><b>Tidak Ada Data</b></center>";
					}
					else
					{
						$data_daftar_profil = mysql_query("SELECT * FROM profil ORDER BY judul");
						while($ambil_data_daftar_profil = mysql_fetch_array($data_daftar_profil))
						{
							echo "<p><a class='link_2' href='profil-$ambil_data_daftar_profil[id_profil].html'><i class='icofont icofont-curved-right' aria-hidden='true' style='padding-right: 10px;'></i>$ambil_data_daftar_profil[judul]</a></p>";
						}
					}
					?>
				</div>
			</div>
			<br/>
			<div class="panel panel-primary shadow" style="border: none;">
				<div class="panel-heading" style="background-color: #A73017; border: none;"><h4><b>Lokasi</b><i class="icofont icofont-social-google-map fa-lg" aria-hidden="true" style="padding-left: 15px;"></i></h4></div>
				<div class="panel-body" style="background-color: #FAFAD6;">
					<iframe class="bordered" src="<?=$ambil_data_konfigurasi['peta'];?>" width="100%" height="300" frameborder="1" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
			<div class="rb-wrap">
				<h3 class="ribbon">
					<strong class="ribbon-content">Kontak</strong>
				</h3>
			</div>
			<br/>
			<table class="table table-hover">
				<tr>
					<th width="20%">Telepon</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['telepon'];?></td>
				</tr>
				<tr>
					<th>Email</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['email'];?></td>
				</tr>
				<tr>
					<th>Alamat</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['alamat'];?></td>
				</tr>
				<tr>
					<th>Facebook</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['facebook'];?></td>
				</tr>
				<tr>
					<th>Instagram</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['instagram'];?></td>
				</tr>
				<tr>
					<th>Twitter</th>
					<th>:</th>
					<td><?=$ambil_data_konfigurasi['twitter'];?></td>
				</tr>
			</table>
			
			<br/>
			<div class="rb-wrap">
				<h3 class="ribbon">
					<strong class="ribbon-content">Atau Kirim Pesan</strong>
				</h3>
			</div>
			<br/>
			
			<center>
				<form class="form-horizontal" role="form" method="post" action="index.php?link=page/kontak.php">	
					<table class="table table-hover">
						<tr>
							<th width="20%">Nomor Telepon</th>
							<th>:</th>
							<td>
								<input type="text" class="form-control hanya_angka" name="nomor_pengirim" maxlength="15" required/>
							</td>
						</tr>
						<tr>
							<th>Email</th>
							<th>:</th>
							<td>
								<input type="text" class="form-control tanpa_spasi" name="email_pengirim" maxlength="35"/>
							</td>
						</tr>			
						<tr>
							<th>Nama</th>
							<th>:</th>
							<td><input type="text" class="form-control" name="nama_pengirim" maxlength="50" required/></td>
						</tr>	
						<tr>
							<th>Pesan</th>
							<th>:</th>
							<td>
								<textarea id="textarea" class="form-control" rows="3" name="pesan" maxlength="250" required></textarea>
								<div id="hitung_sisa"></div>
							</td>
						</tr>
						<tr>
							<th colspan="2"></th>
							<td>
								<p><img src="page/captcha.php" alt="Kode Captcha" style="width:200px; height:32px;"></p>
								<p><input class="form-control tanpa_spasi" name="validasi" style="width:200px;" maxlength="5" required/></p>
							</td>
						</tr>						
						<tr>
							<td colspan="3">
								<br/>
								<center>
									<button type="submit" class="btn btn-primary" name="simpan_pesan"><i class="fa fa-send" aria-hidden="true" style="padding-right: 10px;"></i>Kirim Pesan</button>
								</center>
							</td>
						</tr>	
						<script>
							$("input.tanpa_spasi").on({
								keydown: function(e){
								if (e.which === 32)
								return false;
								},
								change: function(){
								this.value = this.value.replace(/\s/g, "");
								}
							});
						</script>	
						<script>
							$("input.hanya_angka").keydown(function(event){
								if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode == 67 && event.ctrlKey === true) || (event.keyCode == 88 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)){
									return;
								}
								if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)){
									event.preventDefault();
								}
							});
						</script>		
					</table>
				</form>
			</center>
		</div>
	</div>
	
<?php
}
?>