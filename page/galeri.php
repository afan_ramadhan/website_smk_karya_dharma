<script language="JavaScript">
	function GantiTitle(){
		document.title="<?=$ambil_data_konfigurasi['nama_sekolah'];?> | Galeri";
	}
	GantiTitle();
</script>

<br/>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="rb-wrap">
			<h3 class="ribbon">
				<strong class="ribbon-content">Galeri</strong>
			</h3>
		</div>
		<br/>
		<?php
		$jml_data = mysql_num_rows (mysql_query("SELECT * FROM galeri"));
		if ($jml_data==0)
		{
			echo "<h3 align='center'>Tidak Ada Data</h3>";
		}
		else
		{
			$dataPerPage = 15; 
			if(isset($_GET['hal']))
			{	
				$noPage = $_GET['hal'];
			}
			else
			{	
				$noPage = 1;
			}
			$offset = ($noPage - 1) * $dataPerPage;
			$ambil_data = mysql_query("SELECT * FROM galeri ORDER BY id_galeri DESC LIMIT $offset, $dataPerPage");
			$hitung_record = mysql_query("SELECT COUNT(*) AS jumData FROM galeri");
			$data = mysql_fetch_array($hitung_record);
			$jumData = $data['jumData'];
			$jumPage  = ceil($jumData/$dataPerPage);
			while($hasil_data = mysql_fetch_array($ambil_data))
			{
				echo "
				<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4' style='padding-bottom: 30px;'>
					<a data-lightbox='roadtrip' href='gambar/galeri/".$hasil_data['gambar']."' title='".$hasil_data['keterangan']."'>
						<img class='img-thumbnail animasi_gerak animasi_gelap' src='gambar/galeri/optimized_$hasil_data[gambar]' alt='$hasil_data[nama]'>
					</a>
				</div>";
			}
			echo "
			</div>
			<div class='text-center'>
				<ul class='pager'>";
					$link = "galeri-hal-";
					if ($noPage > 1) echo  "<li><a href='".$link.($noPage-1).".html'><i class='fa fa-arrow-left' aria-hidden='true'></i></a></li>";
					for($jml_hal = 1; $jml_hal <= $jumPage; $jml_hal++)
					{
						if($noPage == $jml_hal)
						{
							echo "<li class='disabled'><a href='' style='cursor'>".$jml_hal."</a></li>";
						}
						else
						{
							echo "<li><a href='".$link.$jml_hal.".html'>".$jml_hal."</a></li>";
						}
					}
					if ($noPage < $jumPage) echo "<li><a href='".$link.($noPage+1).".html'><i class='fa fa-arrow-right' aria-hidden='true'></i></a></li>";
				echo "
				</ul>
			</div>";
		}
		?>
	</div>
</div>

<br/>