<div id="slider">
	<img class="img-responsive" src="gambar/slideshow/slide_1.jpg" data-src-2x="img/1@2x.jpg" data-caption="#caption_1"/>
	<img class="img-responsive" src="gambar/slideshow/slide_2.jpg" data-src-2x="img/2@2x.jpg" data-caption="#caption_2"/>
	<img class="img-responsive" src="gambar/slideshow/slide_3.jpg" data-src-2x="img/3@2x.jpg" data-caption="#caption_3"/>
	<img class="img-responsive" src="gambar/slideshow/slide_4.jpg" data-src-2x="img/4@2x.jpg" data-caption="#caption_4"/>
	<img class="img-responsive" src="gambar/slideshow/slide_5.jpg" data-src-2x="img/4@2x.jpg" data-caption="#caption_5"/>
</div>
	
<div id="caption_1" style="display:none;">
	<font face="font_2">
		<h2 align="center">Selamat Datang Di Website</h2>
		<h2 align="center">SMK Karya Dharma 1 Abung Selatan</h2>
		<br/>
		<?php
		$data_profil = mysql_query("SELECT * FROM profil ORDER BY id_profil");
		while($ambil_data_profil = mysql_fetch_array($data_profil))
		{
			echo "<h4><a href='profil-$ambil_data_profil[id_profil].html'><i class='fa fa-thumbs-o-up' aria-hidden='true' style='padding-right: 10px;'></i>$ambil_data_profil[judul]</a></h4>";
		}
		?>
	</font>
</div>
	
<div id="caption_2" style="display:none;">
	<font face="font_2">
		<h2 align="center">Program Keahlian :</h2>
		<br/>
		<h4><i class="fa fa-puzzle-piece" aria-hidden="true" style="padding-right: 10px;"></i>Teknik Pemesinan</a></h4>
		<h4><i class="fa fa-puzzle-piece" aria-hidden="true" style="padding-right: 10px;"></i>Teknik Sepeda Motor</a></h4>
		<h4><i class="fa fa-puzzle-piece" aria-hidden="true" style="padding-right: 10px;"></i>Teknik Kendaraan Ringan</a></h4>
		<h4><i class="fa fa-puzzle-piece" aria-hidden="true" style="padding-right: 10px;"></i>Teknik Komputer Dan Jaringan</a></h4>
		<h4><i class="fa fa-puzzle-piece" aria-hidden="true" style="padding-right: 10px;"></i>Teknik Instalasi Pemanfaatan Tenaga Listrik</a></h4>
	</font>
</div>

<div id="caption_3" style="display:none;">
	<font face="font_2">
		<h2 align="center">Program Keahlian (Kesehatan) :</h2>
		<br/>
		<h3><i class="fa fa-stethoscope" aria-hidden="true" style="padding-right: 10px;"></i>Keperawatan</a></h3>
		<h3><i class="fa fa-stethoscope" aria-hidden="true" style="padding-right: 10px;"></i>Farmasi</a></h3>
		<h3><i class="fa fa-stethoscope" aria-hidden="true" style="padding-right: 10px;"></i>Analis Kesehatan</a></h3>
	</font>
</div>
	
<div id="caption_4" style="display:none;">
	<font face="font_2">
		<h2 align="center">Fasilitas Dan Keunggulan :</h2>
		<br/>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Ruang Kelas Berlantai Tiga Yang Nyaman.</a></h4>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Laboraturium Komputer Dengan Fasilitas AC Dan Jaringan Internet.</a></h4>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Masjid Ar Rachma Sebagai Sarana Peningkatan Imtaq bagi Seluruh Siswa (Rohis).</a></h4>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Bengkel Praktik Dengan Peralatan Dan Perlengkapan Yang Lengkap.</a></h4>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Sarana Olahraga Yang Luas, Meliputi Lapangan Basket, Voli, Sepak Bola, Bulu Tangkis, Dan Beladiri.</a></h4>
		<h4><i class="fa fa-star" aria-hidden="true" style="padding-right: 10px;"></i>Alumni SMK Karya Dharma 1 Telah Banyak Diterima Diperusahaan-Perusahaan Terkemuka Di Indonesia.</a></h4>
	</font>
</div>
	
<div id="caption_5" style="display:none;">
	<font face="font_2">
		<h2 align="center">Prestasi :</h2>
		<br/>
		<h4><i class="fa fa-trophy" aria-hidden="true" style="padding-right: 10px;"></i>Juara 1 Liga Pelajar Indonesia Tahun 2012 Dan 2013.</a></h4>
		<h4><i class="fa fa-trophy" aria-hidden="true" style="padding-right: 10px;"></i>Juara 2 Lomba Teknologi Tepat Guna Tahun 2011 Dan 2012.</a></h4>
		<h4><i class="fa fa-trophy" aria-hidden="true" style="padding-right: 10px;"></i>Juara Voli Lomba Keterampilan Siswa Tahun 2009.</a></h4>
		<h4><i class="fa fa-trophy" aria-hidden="true" style="padding-right: 10px;"></i>Juara Umum Lomba Keterampilan Siswa Tahun 2007.</a></h4>
		<h4><i class="fa fa-trophy" aria-hidden="true" style="padding-right: 10px;"></i>Juara 3 LKS Siswa SMK Teknologi Se-Lampung Utara Tahun 2015.</a></h4>
	</font>
</div>