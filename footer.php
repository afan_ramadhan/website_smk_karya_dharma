<div class="footer_1">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
				<h2 class="font_2" style="line-height: 15px;"><b>Sekolah</b></h2>
				<h2 class="font_2" style="line-height: 15px;"><b>Menengah</b></h2>
				<h2 class="font_2" style="line-height: 15px;"><b>Kejuruan</b></h2>
				<br/>
				<h1 class="font_2"><b><?=$ambil_data_konfigurasi['nama_sekolah'];?></b></h1>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<h4><b>Alamat</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['alamat'];?></p>
								
				<h4><b>Telepon</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['telepon'];?></p>
				
				<h4><b>Email</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['email'];?></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<h4 style="padding-bottom: 5px;"><b>Statistik</b></h4>
				<div style="padding-bottom: 5px;">
					<?php
					$ip = $_SERVER['REMOTE_ADDR'];
					$tanggal = date("Ymd");
					$waktu = time();

					$s = mysql_query("SELECT * FROM statistics WHERE ip = '$ip' AND tanggal = '$tanggal'");

					if(mysql_num_rows($s) == 0)
					{
						mysql_query("INSERT INTO statistics VALUES ('$ip', '$tanggal', '1', '$waktu')");
					} 
					else
					{
						mysql_query("UPDATE statistics SET hits = hits+1, online = '$waktu' WHERE ip = '$ip' AND tanggal = '$tanggal'");
					}

					$pengunjung = mysql_num_rows(mysql_query("SELECT * FROM statistics WHERE tanggal = '$tanggal' GROUP BY ip"));
					$totalpengunjung = mysql_result(mysql_query("SELECT COUNT(hits) FROM statistics"), 0); 
					$hits = mysql_fetch_assoc(mysql_query("SELECT SUM(hits) AS hitstoday FROM statistics WHERE tanggal = '$tanggal' GROUP BY tanggal")); 
					$totalhits = mysql_result(mysql_query("SELECT SUM(hits) FROM statistics"), 0); 
					$tothitsgbr = mysql_result(mysql_query("SELECT SUM(hits) FROM statistics"), 0); 
					$bataswaktu = time() - 300;
					$pengunjungonline = mysql_num_rows(mysql_query("SELECT * FROM statistics WHERE online > '$bataswaktu'"));

					$path = "gambar/statistics/";
					$ext = ".png";

					$tothitsgbr = sprintf("%06d", $tothitsgbr);
					for ($i = 0; $i <= 9; $i++)
					{
						$tothitsgbr = str_replace($i, "<img src='$path$i$ext' alt='$i'> ", $tothitsgbr);
					}

					echo "
					<font size='-1'>
						<table>
							<tr>
								<td align='center'>$tothitsgbr</td>
							</tr>
							<tr>
								<td><br/></td>
							</tr>
							<tr>
								<td><img src='gambar/statistics/hariini.png' style='padding-right: 10px;'/>Pengunjung hari ini : $pengunjung</td>
							</tr>
							<tr>
								<td><img src='gambar/statistics/total.png' style='padding-right: 10px;'/>Total pengunjung : $totalpengunjung</td>
							</tr>
							<tr>
								<td><img src='gambar/statistics/hariini.png' style='padding-right: 10px;'/>Hits hari ini : $hits[hitstoday]</td>
							</tr>
							<tr>
								<td><img src='gambar/statistics/total.png' style='padding-right: 10px;'/>Total Hits : $totalhits</td>
							</tr>
							<tr>
								<td><img src='gambar/statistics/online.png' style='padding-right: 10px;'/>Pengunjung Online : $pengunjungonline</td>
							</tr>
						</table>
					</font>";
					?>
				</div>
				
				<h4 style="padding-bottom: 5px;"><b>Banner</b></h4>
				<center>
					<a data-toggle="modal" data-target="#Banner">
						<img class="img-responsive animasi_terang" src="gambar/banner/banner.jpg"/>
					</a>
					<div class="modal fade" id="Banner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body">
									<img class="img-responsive" src="gambar/banner/Ghuroba Website Developer.jpg"/>
								</div>
							</div>
						</div>
					</div>
				</center>
			</div>
		</div> 
	</div>
</div>	

<div class="footer_2">
	<div class="container">
		<p align="center">Copyright <i class="fa fa-copyright" aria-hidden="true"></i> <?=date('Y');?>. <?=$ambil_data_konfigurasi['nama_sekolah'];?>. All Rights Reserved.</p>
	</div>
</div>