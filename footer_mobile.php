<div class="footer_1">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
				<h2 class="font_2" style="line-height: 15px;"><b>Sekolah</b></h2>
				<h2 class="font_2" style="line-height: 15px;"><b>Menengah</b></h2>
				<h2 class="font_2" style="line-height: 15px;"><b>Kejuruan</b></h2>
				<br/>
				<h1 class="font_2"><b><?=$ambil_data_konfigurasi['nama_sekolah'];?></b></h1>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<h4><b>Alamat</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['alamat'];?></p>
								
				<h4><b>Telepon</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['telepon'];?></p>
				
				<h4><b>Email</b></h4>
				<p style="padding-bottom: 10px;"><?=$ambil_data_konfigurasi['email'];?></p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<h4 style="padding-bottom: 5px;"><b>Banner</b></h4>
				<center>
					<a data-toggle="modal" data-target="#Banner">
						<img class="img-responsive animasi_terang" src="gambar/banner/banner.jpg"/>
					</a>
					<div class="modal fade" id="Banner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body">
									<img class="img-responsive" src="gambar/banner/Ghuroba Website Developer.jpg"/>
								</div>
							</div>
						</div>
					</div>
				</center>
			</div>
		</div> 
	</div>
</div>	

<div class="footer_2">
	<div class="container">
		<p align="center">Copyright <i class="fa fa-copyright" aria-hidden="true"></i> <?=date('Y');?>. <?=$ambil_data_konfigurasi['nama_sekolah'];?>. All Rights Reserved.</p>
	</div>
</div>