<nav class="navbar navbar-custom-2 static-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="home.html"><i class="fa fa-home fa-lg" aria-hidden="true" style="padding-right: 15px;"></i><b>Beranda</b></a>
		</div>
		<div>
			<ul class="nav navbar-nav navbar-right">
				<?php
				if(!empty($ambil_data_konfigurasi['facebook']))
				{
					echo "
					<li>
						<a href='http://$ambil_data_konfigurasi[facebook]'><i class='fa fa-facebook fa-lg' aria-hidden='true'></i></a>
					</li>";
				}
				if(!empty($ambil_data_konfigurasi['twitter']))
				{
					echo "
					<li>
						<a href='http://$ambil_data_konfigurasi[twitter]'><i class='fa fa-twitter fa-lg' aria-hidden='true'></i></a>
					</li>";
				}
				if(!empty($ambil_data_konfigurasi['instagram']))
				{
					echo "
					<li>
						<a href='http://$ambil_data_konfigurasi[instagram]'><i class='fa fa-instagram fa-lg' aria-hidden='true'></i></a>
					</li>";
				}
				?>
			</ul>
		</div>
	</div>
</nav>

<div class="container">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<a href="home.html">
				<img class="img-responsive animasi_gelap" src="gambar/konfigurasi/<?=$ambil_data_konfigurasi['gambar_header'];?>" alt="Header <?=$ambil_data_konfigurasi['nama_sekolah'];?>">
			</a>
		</div>
	</div>
</div>