/* Tombol Scroll To Top --------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function(){
	// Show or hide the sticky footer button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 200){
			$('.go-top').fadeIn(200);
		}
		else{
			$('.go-top').fadeOut(200);
		}
	});	
	
	// Animate the scroll to top
	$('.go-top').click(function(event){
		event.preventDefault();	
		$('html, body').animate({scrollTop: 0}, 300);
	})
});

/* Hitung Sisa -----------------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function(){
	var text_max = 250;
	$('#hitung_sisa').html(text_max + ' Karakter Tersisa.');

	$('#textarea').keyup(function(){
		var text_length = $('#textarea').val().length;
		var text_remaining = text_max - text_length;

		$('#hitung_sisa').html(text_remaining + ' Karakter Tersisa.');
	});
});

/* Preview Sebelum Upload ------------------------------------------------------------------------------------------------------------------------------------*/
function PreviewImage() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("input_gambar").files[0]);
	oFReader.onload = function(oFREvent){
		document.getElementById("uploadPreview").src = oFREvent.target.result;
	};
};
function PreviewImage1() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("input_gambar_1").files[0]);
	oFReader.onload = function(oFREvent){
		document.getElementById("uploadPreview1").src = oFREvent.target.result;
	};
};
function PreviewImage2() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("input_gambar_2").files[0]);
	oFReader.onload = function(oFREvent){
		document.getElementById("uploadPreview2").src = oFREvent.target.result;
	};
};

/* Tombol Kembali --------------------------------------------------------------------------------------------------------------------------------------------*/
function goBack(){
	window.history.back();
}

/* Menghilangkan Fungsi Spasi Di Input -----------------------------------------------------------------------------------------------------------------------*/
$("input.tanpa_spasi").on({
	keydown: function(e){
		if (e.which === 32)
		return false;
	},
	change: function(){
		this.value = this.value.replace(/\s/g, "");
	}
});

/* Membuat Input Hanya Angka ---------------------------------------------------------------------------------------------------------------------------------*/
$("input.hanya_angka").keydown(function(event){
	if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode == 67 && event.ctrlKey === true) || (event.keyCode == 88 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)){
		return;
	}
	if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)){
		event.preventDefault();
	}
});

/* Tampilkan Hapus Terpilih ----------------------------------------------------------------------------------------------------------------------------------*/
$(function(){
	$('.terpilih').click(function(){
		if($('.terpilih').is(':checked')){
			$('#hapus_terpilih').show(500);
			$('.tampil_terpilih').show(500);
		}
		else{
			$('#hapus_terpilih').hide(500);
			$('.tampil_terpilih').hide(500);
		}
	});
});

/* Hitung Terpilih -------------------------------------------------------------------------------------------------------------------------------------------*/
$(document).ready(function(){
	var data_terpilih = 0;
	$('.terpilih').click(function(){
		if($(this).prop('checked')==true){
			data_terpilih++;
		}
		else{
			data_terpilih--;
		}
		$('#hitung_terpilih').html(data_terpilih + ' Data Terpilih');
	});
});
