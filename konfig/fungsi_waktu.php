<?php
date_default_timezone_set('Asia/Jakarta');
$seminggu = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
$hari = date("w");
$hari_ini = $seminggu[$hari];

$tanggal_sekarang = date("d/m/Y");
$tanggal = date("d");
$bulan = date("m");
$bulan = ltrim($bulan, '0');
$tahun = date("Y");

$setahun = array("fix", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
$bulan_sekarang = $setahun[$bulan];

$jam_sekarang = date("H:i:s");
?>
